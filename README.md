# README

This file will describe the minimum requirements and purpose of these scripts

## What is this repository for?

* Each of these shell scripts automate a small task within a UNIX system to make my life easier
* I've added these files to this public repo for the community to use and contribute if they
find an easier/cleaner implementation
* None of the scripts have a version number and I'll be working on them a little bit every night to get
initial versions on each file
* Scripts will be pushed to this repo as other ideas come to mind that will make my life easier
* I am NOT responsible if your machine breaks, crashes, starts a nuclear war because you cannot read
instructions on how to install and execite the scripts correctly

## Description of scripts

#### exec-paramter-skel

* Contains skeleton code for creating scripts that take only 1 paramter

#### exec-parameter-opts-skel

* Contains skeleton code for creating script that take multiple parameters and execute them all in a while loop

#### create-shortcut

* Usage: `./create-shortcut <options>`
* Creates a .desktop shortcut file and depending if it's a short for just the user or system (all users), it will
install the shortcut in its appropriate place

#### host-file

* Usage: `./host-file <options>`
* Installs, updates or uninstalls the /etc/hosts file on the system

#### pie-info

* Usage: `./pie-info`
* I was not the original author of this file and am not taking credit for it's creation.  I copied the code from the ~/.bashrc
file within my Raspian Wheezy RPi image.
* This file prints out system info such as GPU and CPU temps

#### start-ssh-agent

* Starts and ssh-agent via CLI
* The code within that file needs to go at the bottom of the ~/.bashrc file so it can start when ever a new terminal or shell
is open
* Once a ssh-agent is running, it will not open another one unless the process of the current one is killed

#### ez_setup.py

* Usage: `python ez_setup.py`
* I was not the original author of this file and am not taking credit for it's creation
* This allows "pip" to be installed so pip modules can be installed easier from CLI
* I ran this in my Windows Cygin shell and it seems to be working as expected, no errors or anomalies yet

## Minimum Requirements

* So far specific scripts have only been tested on the platforms listed below:

	* RPi (Raspberry Pi and Raspberry Pi 2) RetroPie
		* pie-info
		
	* Ubuntu 14.04 LTS and Debian 8 (x64)
		* create-shortcut
		* host-file
		* start-ssh-agent code copied into ~/.bashrc file
		
	* Cygwin (x86) on Windows 7 Ultimate (x64)
		* start-ssh-agent code copied into ~/.bashrc file
		* ez_setup.py
	
## Dependencies

* From the top of my head the only dependencies that I remember is being able to use `sudo`, `wget` and `python`.
Will add more if/when I remember or if the community runs them and gets errors because they don't have all the
required dependencies.
	
## Installation/Deployment Instructions

* The following scripts are in the following paths in my environment and I've had no issues.  The paths
that I use are recommended since it seems stable and diagnosing issues will be easier since I don't know
how your environment has been configured
* The paths that I used to deploy/install these scripts are in the parentesis next to the environments I have
listed below

* UNIX Environment (/usr/local/bin)
	* pie-info
	* create-shortcut
	* host-file
	
* UNIX and Cygwin Environment (within ~/.bashrc file)
	* Contents of the start-ssh-agent script
	
* Cygwin Environment (/usr/bin)
	* pie-info
	* host-file

## Execution

* Once the instructions from the "Installation/Deployment Instructions" have been completed correctly, just
the script name needs to be typed in the terminal shell and the script should run without errors

## Contribution guidelines

* Testing and finding bugs
* Writing tests
* Code review
* Contributing ideas/code

## Who do I talk to?

* Myself, repo owner, team member or admin
