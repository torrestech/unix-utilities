#!/bin/bash

# Function that creates desktop shortcut
# Takes 5 parameters
# 1 = name of .desktop file, #2 = name of app, #3 = comment of app, #4 is exe location, #5 icon location, #6 categories
function create_desktop_shortcut(){
	echo "#!/usr/bin/env xdg-open" > ~/Desktop/"$1".desktop
	echo "[Desktop Entry]" >> ~/Desktop/"$1".desktop
	echo "Encoding=UTF-8" >> ~/Desktop/"$1".desktop
	echo "Name=$2" >> ~/Desktop/"$1".desktop
	echo "Comment=$3" >> ~/Desktop/"$1".desktop
	echo "Exec=$4" >> ~/Desktop/"$1".desktop
	echo "Icon=$5" >> ~/Desktop/"$1".desktop
	echo "Terminal=false" >> ~/Desktop/"$1".desktop
	echo "Type=Application" >> ~/Desktop/"$1".desktop
	echo "Categories=GNOME;Application;$6" >> ~/Desktop/"$1".desktop
	echo "StartupNotify=true">> ~/Desktop/"$1".desktop
}

# Function that prompts the user for shortcut application info
function prompt_user(){
	strFileName=""
	strAppName=""
	strComment=""
	strExePath=""
	strIconPath=""
	strCategories=""

	echo -n "Enter file name (w/o '.desktop' extension): "
	read strFileName

	echo -n "Enter app name: "
	read strAppName

	echo -n "Enter comment: "
	read strComment

	echo -n "Enter executable path: "
	read strExePath

	echo -n "Enter icon path: "
	read strIconPath

	echo -n "Enter categories (separated with ';'): "
	read strCategories

	create_desktop_shortcut "$strFileName" "$strAppName" "$strComment" "$strExePath" "$strIconPath" "$strCategories"
}

# Function that moves .desktop file to /usr/share/applications
function move_to_system(){
	sudo mv ~/Desktop/*.desktop /usr/local/share/applications/
}

# Function that moves .desktop file to ~/.local/share/applications
function move_to_user(){
	mv ~/Desktop/*.desktop ~/.local/share/applications/
}

# Prints out the help options
function help_options(){
	echo "Usage: $0 <options>"
	echo ""
	echo "options:"
	echo -e "\t-user:\t\tcreate and install shortcut for user"
	echo -e "\t-system:\tcreate and install shortcut for system"
	echo -e "\t-help:\t\tthis menu"
}

# Function that makes sure the second parameter is set or else it notifies the user and exits the script
# Takes 2 parameters
# 1 = options, 2 = option parameter
function check_2nd_param(){
	if [[ -z $2 ]];then
		exit_error "$1 has EMPTY parameter, exiting!"
	fi
}

# Function that exits the script and takes an error message as the first parameter
# Takes 1 parameter
# 1 = error message
function exit_error(){
	echo $1
	exit -1
}

#####################################################################
#			Beginning of script
#####################################################################
case $1 in
	-user)
		prompt_user
		move_to_user
	;;
	-system)
		prompt_user
		move_to_system
	;;
	-help)
		help_options
	;;
	*)
		help_options
	;;
esac

