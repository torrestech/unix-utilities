# README for Cygwin Setup

* Below are steps that I've done on my personal machines to get Cygwin as close as I can to a UNIX type shell
environment
* I went as far as getting packages from the Cygwin setup-x86.exe, modifying the ~/.bashrc file and symblinked
certain programs within the Windows environment into Cygwin

## Packages from setup-x86.exe

* Archive/p7zip
* Devel/gcc
* Devel/g++
* Devel/subversion
* Devel/git
* Devel/make
* Devel/cmake
* Editors/nano
* Net/openssh
* Net/curl
* Net/inetutils
* Python/python
* Utils/ncurses
* Web/wget

## Tweaks done to ~/.bashrc file

* Uncomment or add line below
```sh
alias ls='ls -hF --color=tty'                 # classify files in colour
```

* Add lines below to have path set in title of xterm shell and customize PS1 variables
```sh
# PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ ' #Green
PS1='\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ' #Red

# If this is an xterm set the title to user@host:dir
case "$TERM" in
    xterm*|rxvt*)
        PS1="\[\e]0;\u@\h: \W\a\]$PS1"
        ;;
    *)
        ;;
esac
```

* Add lines below to have ssh-agent start on a new shell instance so GIT does not prompt you every time
for a passphrase when pulling or pushing code
```sh
# Note: ~/.ssh/environment should not be used, as it
#       already has a different purpose in SSH.

env=~/.ssh/agent.env

# Note: Don't bother checking SSH_AGENT_PID. It's not used
#       by SSH itself, and it might even be incorrect
#       (for example, when using agent-forwarding over SSH).

agent_is_running() {
	if [[ ${SSH_AUTH_SOCK} ]]; then
        # ssh-add returns:
        #   0 = agent running, has keys
        #   1 = agent running, no keys
        #   2 = agent not running
        ssh-add -l > /dev/null 2>&1 || [ $? -eq 1 ]
    else
        false
    fi
}

agent_has_keys() {
    ssh-add -l > /dev/null 2>&1
}

agent_load_env() {
    . "${env}" 2&> /dev/null
}

agent_start() {
	echo -n "Initializing new SSH agent ... "
    (umask 077; ssh-agent -s | sed 's/^echo/#echo/' > "${env}")
	echo "SUCCESS!"
    . "${env}" > /dev/null
}

if ! agent_is_running; then
	agent_load_env
fi

# If your keys are not stored in ~/.ssh/id_rsa or ~/.ssh/id_dsa, you'll need
# to paste the proper path after ssh-add
if ! agent_is_running; then
    agent_start
    ssh-add
elif ! agent_has_keys; then
    ssh-add
fi

unset env
```

* Add lines below to set the `EDITOR` variable
```sh
export EDITOR=nano
```

## Scripts that need to be pulled or programs that need to be symlinked into /usr/bin of Cygwin environment

* From home dir in cygwin:
```bash
$ cd /usr/bin
$ ln -s /cygdrive/c/Program\ Files\ \(x86\)/Notepad++/notepad++.exe notpad++
```

